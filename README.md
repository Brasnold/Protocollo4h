# Protocollo di 4 ore

Template utile per effettuare una competizione lampo di quattro ore. Si assume che il progetto sia non strutturato, cioè che tutti i file siano in un'unica directory. L'obbiettivo è quello di effettuare analisi esplorativa, training, validation e prediction in questo unico file. La filosofia di questo progetto è quindi quella di definire tutte le variabili temporanee dentro ai metodi in modo che vadano subito distrutte oppure di rimuoverle manualmente non appena non siano più necessarie.

